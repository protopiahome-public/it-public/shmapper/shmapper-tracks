<?php
/**
 * Plugin Name: ShMapper Tracks
 * Plugin URI: http://genagl.ru/?p=652
 * Description: Add-on for Shmapper by Teplitza plugin. Added paths, tracks and routes functionality
 * Version: 1.0.0 
 * Requires PHP: 5.6
 * Author: KB of Protopia Home
 * Author URI:  https://kb.protopia-home.ru
 * License: GPL2
 * Text Domain: shmapper-tracks
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Contributors:
	Genagl (genag1@list.ru) 

 * License: GPLv2 or later
	Copyright 2021  Genagl  (email: genag1@list.ru)

	GNU General Public License, Free Software Foundation <http://www.gnu.org/licenses/gpl-2.0.html>

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/** Load textdomain */
function init_textdomain_shmapper_tracks() {
	if ( function_exists('load_textdomain') ) {
		load_textdomain( 'shmapper-tracks', WP_PLUGIN_DIR . '/' . dirname( plugin_basename( __FILE__ ) ) . '/lang/shmapper-tracks-' . get_locale() . '.mo' );
	}

	if ( function_exists( 'load_plugin_textdomain' ) ) {
		load_plugin_textdomain( 'shmapper-tracks', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
	}
}
add_action('plugins_loaded', 'init_textdomain_shmapper_tracks');

// Paths.
define( 'SHMTRACKS_URLPATH', WP_PLUGIN_URL . '/' . plugin_basename( dirname(__FILE__) ) . '/' );
define( 'SHMTRACKS_REAL_PATH', WP_PLUGIN_DIR . '/' . plugin_basename(dirname(__FILE__) ) . '/' );
define( 'SHMAPPER_TRACKS', 'shmapper-tracks' );
define( 'SHM_TRACK_TYPE', 'shmapper_track_type' );
define( 'SHMAPPER_TRACKS_TRACK', 'shmapper_track' );
define( 'SHMAPPER_TRACKS_POINT', 'shmapper_track_point' );
define( 'SHMAPPER_TRACKS_DRAW', 'shmapper_track_draw' );
define( 'SHMAPPER_TRACKS_VERSION', '1.0.03' );

require_once ABSPATH . 'wp-admin/includes/plugin.php';
$pl = is_plugin_active( 'shmapper-by-teplitsa/shmapper.php' ) || is_plugin_active( 'shmapper-protopia-home/shmapper.php' );
if ( !$pl ) 
{
	add_action( 'admin_notices', 'after_install_sticker_shm' ); 
	return;

}
	
function after_install_sticker_shm()
{
	echo "<style>		
		.shm_notice
		{
			position:relative; 
			display: block;  
			background-color:rgba(240, 240, 240, 0.5);
			border:solid 1px #EFEFEF;  
			line-height: 1.3; 
			vertical-align: middle;  
			padding: 30px!important;	
			color:black;
			font-size:22px;
			font-family:Verdana, sans-serif;
			margin-bottom:4px; 
		}
		.shm_notice h3
		{
			display:flex;
			align-items:center;
			padding:0;
			margin:0;
		}
		.shm_notice h3>b
		{
			color:#2271b1;
			padding-right: 10px;
		}
		.shm_notice_close 
		{
			width: 25px;
			height: 25px;
			cursor: pointer;
			background-image: url(../img/close.svg);
			background-size: 21px 21px;
			background-position: center center;
			float: right;
			text-align: center;
			position: absolute;
			top: 10px;
			right: 10px;
			line-height: 1;
		}
		.shm_notice_close:hover
		{
			background-color:#DDD;
		}
	</style>
	<div class='updated notice shm_notice' id='install_smco_notice' >	
		<span class='shm_notice_close'>
			x
		</span>	
		<h3>
			<img src='".SHMTRACKS_URLPATH."assets/img/shmapper_32x32.svg' /> ". 
			__( "<b>Shmapper </b> is not active.", SHMAPPER_TRACKS ). 
		"</h3>
		<p>" .
			__( "To enable the full functionality of <b>Shmapper Tracks</b>, you need to install the <b>Shmapper</b> plugin.", SHMAPPER_TRACKS ) .
		"</p>
	</div>
	<script>
		jQuery(document).ready(function($)
		{				
			$('.shm_notice_close').on('click', e =>
			{ 
				$(e.currentTarget).parents('.shm_notice').fadeOut('slow');
			})
		})
	</script>
	";
}

require_once SHMTRACKS_REAL_PATH . 'class/ShMapperTracks.class.php';
require_once SHMTRACKS_REAL_PATH . 'class/ShMapperTracksAjax.class.php'; 
require_once SHMTRACKS_REAL_PATH . 'class/ShMaperTrack.class.php'; 
require_once SHMTRACKS_REAL_PATH . 'class/ShMapperTracksPoint.class.php'; 
require_once SHMTRACKS_REAL_PATH . 'class/ShMapTrackType.class.php'; 

register_activation_hook( __FILE__, array( 'ShMapperTracks', 'activate' ) );

if ( function_exists( 'register_deactivation_hook' ) ) 
{
	register_deactivation_hook(__FILE__, array( 'ShMapperTracks', 'deactivate' ) );
}

/** Shamapper-tracks init */
function init_shmapperTracks() 
{
	ShMapperTracks::get_instance(); 
	ShMapperTracksAjax::init();
	ShMapTrackType::init();
	ShMaperTrack::init(); 
	ShMapperTracksPoint::init();  
}
add_action( 'init', 'init_shmapperTracks', 1 );